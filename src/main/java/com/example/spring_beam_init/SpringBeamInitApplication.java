package com.example.spring_beam_init;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBeamInitApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBeamInitApplication.class, args);
    }

}
