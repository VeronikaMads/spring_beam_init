package com.example.spring_beam_init.bean;

import lombok.Getter;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.cglib.core.internal.Function;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.inject.Provider;

//singleton - Определяет один единственный бин для каждого контейнера Spring IoC (используется по умолчанию).
@Component
public class SingletonBean {
    //   @Lookup
    //Аннотация, которая указывает методы поиска, которые должны быть переопределены контейнером для
    // перенаправления их обратно в org.springframework.beans.factory.BeanFactory для вызова getBean.

    @Autowired
    public Function<String, PrototypeBean> function;

    public PrototypeBean getPrototypeBean(String name) {
        return function.apply(name);
    }
}
