package com.example.spring_beam_init.bean;

import com.example.spring_beam_init.bean.servise.MyService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Getter
@Component
public class BeanWithCollection {
    @Autowired
    private List<MyService> services;
}
