package com.example.spring_beam_init.bean;

import org.springframework.cglib.core.internal.Function;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Arrays;
import java.util.List;

@Configuration
public class AppConfig {
    @Scope("prototype")
    @Bean
    public PrototypeBean prototypeBean(String name){
        return new PrototypeBean(name);
    }
    @Bean
    public Function<String,PrototypeBean> prototypeBeanFunction(){
        return name -> prototypeBean(name);
    }
    @Bean
    public List<String > nameListBean(){
        return Arrays.asList("name1","name2","name3");
    }
}
