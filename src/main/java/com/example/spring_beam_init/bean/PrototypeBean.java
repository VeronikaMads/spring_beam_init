package com.example.spring_beam_init.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
//prototype - Позволяет иметь любое количество экземпляров бина.
//@Scope("prototype") //@Scope указывает имя области, которая будет использоваться для экземпляров аннотированного типа.
public class PrototypeBean {
    public PrototypeBean(String name) {
        System.out.println("create");
    }
}
