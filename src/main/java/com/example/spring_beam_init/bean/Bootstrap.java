package com.example.spring_beam_init.bean;

import com.example.spring_beam_init.bean.servise.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Bootstrap implements CommandLineRunner {
    @Autowired
    SingletonBean singletonBean;
    @Autowired
    BeanWithCollection beanWithCollection;
    @Autowired
    public List<String> nameList;

    @Override
    public void run(String... args) throws Exception {

        System.out.println(" first prot "+singletonBean.getPrototypeBean("name1"));
        System.out.println(" second prot "+singletonBean.getPrototypeBean("name2"));
        System.out.println("collection Name List" + nameList.hashCode());
        beanWithCollection.getServices().forEach(MyService::print);
    }
}
