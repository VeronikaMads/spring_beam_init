package com.example.spring_beam_init.bean.servise;

public interface MyService {
    default void print() {
        System.out.println(getName());
    }

    String getName();
}
