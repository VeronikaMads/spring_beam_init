package com.example.spring_beam_init.bean.servise;

import org.springframework.stereotype.Component;

@Component
public class SecondService implements MyService{
    @Override
    public String getName() {
        return this.getClass().getName();
    }
}
